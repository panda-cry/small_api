FROM python:3.9
LABEL authors="gecko"

WORKDIR /app

COPY . /small_app
WORKDIR /small_app
ENV PYTHONPATH "${PYTHONPATH}:/app/small_app"
RUN pip install -r requirements.txt

EXPOSE 5000

ENTRYPOINT ["flask", "run"]